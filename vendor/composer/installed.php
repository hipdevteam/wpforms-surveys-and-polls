<?php return array(
    'root' => array(
        'pretty_version' => 'dev-develop',
        'version' => 'dev-develop',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => 'ae56cbb8039fb16c03bc137b38c8adb5d6fb399b',
        'name' => 'wpforms/surveys-polls',
        'dev' => true,
    ),
    'versions' => array(
        'wpforms/surveys-polls' => array(
            'pretty_version' => 'dev-develop',
            'version' => 'dev-develop',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => 'ae56cbb8039fb16c03bc137b38c8adb5d6fb399b',
            'dev_requirement' => false,
        ),
    ),
);
